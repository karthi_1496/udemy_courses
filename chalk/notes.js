const fs = require('fs');

const getNotes = function () {
    return 'Your notes...'
}

const addNotes = function (title, body) {
    const notes = loadNotes();

    notes.push({
        title: title,
        body: body
    });

    saveNotes(notes)
}

const saveNotes = function (notes) {
    const getData = JSON.stringify(notes);
    fs.writeFileSync('notes.json', getData)



}

const loadNotes = function () {

    try {
        const readBuffer = fs.readFileSync('notes.json');
        const fileString = readBuffer.toString();
        return JSON.parse(fileString);
    } catch (e) {
        return []
    }



}





module.exports = {
    addNotes: addNotes,

    saveNotes: saveNotes
}