const chalk = require('chalk');
const yargs = require('yargs');
const notes = require('./notes');

// console.log(chalk.green.bold('success'))

yargs.version('1.1.0')

yargs.command({
    command: 'add',
    describe: 'add a new note',
    builder: {
        title: {
            describe: 'note title',
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: 'note body',
            demandOption: true,
            type: 'string'
        },
    },
    handler: function (argv) {
        notes.addNotes(argv.title, argv.body)
        // console.log('title=' + argv.title)
      
    }
})


yargs.command({
    command: 'remove',
    describe: 'remove a note',
    handler: function () {
        console.log('remove a note')
    }
})
yargs.command({
    command: 'list',
    describe: 'list a note',
    handler: function () {
        console.log('list a note')
    }
})
yargs.command({
    command: 'read',
    describe: 'read a note',
    handler: function () {
        console.log('read a note')
    }
})


yargs.parse()